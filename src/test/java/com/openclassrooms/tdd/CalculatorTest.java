package com.openclassrooms.tdd;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculatorTest {

    private static Instant startedAt;

    private Calculator calculator;

    @BeforeAll
    public static void initStartingTime() {
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {

        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }

    @BeforeEach
    public void initCalculator() {
        calculator = new Calculator();
    }

    @AfterEach
    public void undefCalculator() {
        calculator = null;
    }

    @Test
    void add_twoPositiveIntegers_returnsTheirSum() {
        int a = 2;
        int b = 3;

        int somme = calculator.add(a, b);

        assertThat(somme).isEqualTo(a + b);
    }

    @ParameterizedTest(name = "{0} + {1} doit être égal à {2}")
    @CsvSource({"1,1,2", "1,2,3", "2,2,4"})
    void add_twoPositiveIntegers_returnsTheirSum(int arg1, int arg2, int expectedResult) {
        final int somme = calculator.add(arg1,arg2);

        assertThat(somme).isEqualTo(expectedResult);
    }

    @Test
    void multiply_twoPositiveIntegers_returnsTheirSum() {
        int a = 2;
        int b = 3;

        int somme = calculator.mul(a, b);

        assertThat(somme).isEqualTo(a * b);
    }

    @ParameterizedTest(name = "{0} x 0 doit être égal à 0")
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void multiply_onePositiveIntegerByZero_returnZero(int arg) {
        final int somme = calculator.mul(arg,0);

        assertThat(somme).isEqualTo(0);
    }

    @Test
    @Timeout(1)
    public void longCalculation_shouldComputeInLessThanOneSecond() {
        calculator.longCalculation();
    }
}
